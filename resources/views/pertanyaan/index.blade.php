@extends('pertanyaan.master')
@section('content')
<div class="panel-body">
	<div class="row">
		<!-- BASIC TABLE -->
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Table Inside No Padding</h3>
			</div>
			<div class="panel-body">
				@if(session('success'))
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<i class="fa fa-check-circle"></i> {{session('success')}}
				</div>
				@endif
				<a href="{{url('/pertanyaan/create')}}" class="btn btn-default mb-2"><i class="fa fa-plus-square"></i>Create</a>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Title</th>
							<th>Content</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@forelse($tanya as $key=>$value)
						<tr>
							<td>{{$key + 1}}</td>
							<td>{{$value->judul}}</td>
							<td>{{$value->isi}}</td>
							<td style="display: flex;">
								<a href="/sanber-laravel/public/pertanyaan/{{$value->id}}" class="btn btn-info">Show</a>
								<a href="/sanber-laravel/public/pertanyaan/{{$value->id}}/edit" class="btn btn-success">Update</a>
								<form href="/sanber-laravel/public/pertanyaan/{{$value->id}}" method="post" action="/sanber-laravel/public/pertanyaan/{{$value->id}}">
									@csrf
									@method('DELETE')
									<input type="submit" class="btn btn-danger" value="delete">
								</form>
							</td>
						</tr>
						@empty
							<tr colspan="3">
								<td>No data.</td>
							</tr>
						@endforelse
					</tbody>
				</table>
			</div>
		</div>
				<!-- END BASIC TABLE -->
	</div>
</div>
@endsection('content')

