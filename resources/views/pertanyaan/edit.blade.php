@extends('pertanyaan.master')

@section('title')
<h3 class="panel-title">Update Data </h3>
@endsection('title')

@section('content')
<div class="panel-body">
	<form method="POST" action="/sanber-laravel/public/pertanyaan/{{$tanya->id}}">
		{{csrf_field()}}
		@method('PUT')
		<div class="col-md-2">
			<input type="submit" name="submit" class="btn btn-primary btn-block"></input>
		</div>

		<br><br><br>
		<label for="judul">Title</label>
		<input class="form-control input-lg" id="judul" name="judul" placeholder="Title" type="text" value="{{old('judul', $tanya->judul)}}" required />

		@error('judul')
		<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<i class="fa fa-warning"></i> Title field is required 
		</div>
		@enderror
		<br>

		<label for="isi">Content</label>
		<input class="form-control input-lg" id="isi" name="isi" placeholder="Content" type="text" rows="5" value="{{old('isi', $tanya->isi)}}" required /></input>

		@error('isi')
		<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<i class="fa fa-warning"></i> Content field is required 
		</div>
		@enderror
		<br>
	</form>
</div>
@endsection('content')
