@extends('pertanyaan.master')

@section('content')
	<div class="panel-body">
		<h1>{{$pertanyaan->judul}}</h1>
		<br>
		<p class="lead">{{$pertanyaan->isi}}</p>
	</div>
@endsection