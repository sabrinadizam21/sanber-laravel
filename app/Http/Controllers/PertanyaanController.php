<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index()
    {
    	$tanya = DB::table('pertanyaan')->get();
    	return view('pertanyaan.index', compact('tanya'));
    }
    public function create()
    {
    	return view('pertanyaan.create');
    }

    function store(Request $request)
    {
      	//dd($request->all());

    	$query = DB::table('pertanyaan')->insert([
    		"judul" => $request["judul"],
    		"isi" => $request["isi"]
    	]);
    	return redirect('/pertanyaan')->with('success', 'Your settings have been succesfully saved');
    }

    public function show($id)
    {
	   	$pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
	   	return view('pertanyaan.show', compact(['pertanyaan']));
    }

    function edit($id){
    	$tanya = DB::table('pertanyaan')->where('id', $id)->first();
    	return view('pertanyaan.edit', compact('tanya'));
    }

    function update($id, Request $request){
     	$query = DB::table('pertanyaan')->where('id', $id)->update([
    		'judul' => $request["judul"],
    		'isi' => $request["isi"]
    	]);
    	return redirect('/pertanyaan')->with('success','Your data have been succesfully edited');
    }

    function destroy($id){
    	$query = DB::table('pertanyaan')->where('id', $id)->delete();
    	return redirect('/pertanyaan')->with('success','Your data have been succesfully deleted');
    }
}
