<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', '\App\Http\Controllers\HomeController@home');
Route::get('/register', '\App\Http\Controllers\AuthController@register');
Route::post('/welcome', '\App\Http\Controllers\AuthController@check_regist')->name('register');

Route::get('/', function(){ return view('adminlte.home');});
Route::get('/data-tables', function(){ return view('adminlte.data-tables');});

Route::prefix('pertanyaan')->group(function(){
	Route::get('/', '\App\Http\Controllers\PertanyaanController@index');
	Route::post('/', '\App\Http\Controllers\PertanyaanController@store')->name('tanya');
	Route::get('/create', '\App\Http\Controllers\PertanyaanController@create');
	Route::get('/{pertanyaan_id}', '\App\Http\Controllers\PertanyaanController@show')->name('show');
	Route::get('{pertanyaan_id}/edit', '\App\Http\Controllers\PertanyaanController@edit');
	Route::put('/{pertanyaan_id}', '\App\Http\Controllers\PertanyaanController@update')->name('update');
	Route::delete('/{pertanyaan_id}', '\App\Http\Controllers\PertanyaanController@destroy');
});
